package com.pandz.stuffmanagement.dto;

import javax.persistence.*;

@Entity
@Table(name = "t_sales_detail")
public class SalesDetailDto {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  private Integer t_sales_id;
  private Integer m_stuff_id;
  private Integer total_stuff;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getT_sales_id() {
    return t_sales_id;
  }

  public void setT_sales_id(Integer t_sales_id) {
    this.t_sales_id = t_sales_id;
  }

  public Integer getM_stuff_id() {
    return m_stuff_id;
  }

  public void setM_stuff_id(Integer m_stuff_id) {
    this.m_stuff_id = m_stuff_id;
  }

  public Integer getTotal_stuff() {
    return total_stuff;
  }

  public void setTotal_stuff(Integer total_stuff) {
    this.total_stuff = total_stuff;
  }
}
