package com.pandz.stuffmanagement.service;

import com.pandz.stuffmanagement.dto.ResponseStatus;
import com.pandz.stuffmanagement.dto.SalesDetailDto;
import com.pandz.stuffmanagement.dto.StuffDto;
import com.pandz.stuffmanagement.repository.SalesDetailRepository;
import com.pandz.stuffmanagement.repository.StuffRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StuffService {
  private final static Logger log = LoggerFactory.getLogger(StuffService.class);

  private StuffRepository stuffRepository;

  @Autowired
  private SalesDetailRepository salesDetailRepository;

  @Autowired
  public StuffService(StuffRepository stuffRepository) {
    this.stuffRepository = stuffRepository;
  }

  // GET ALL STUFF
  public ResponseStatus getAllStuff() {
    log.info("Start Get All Stuff");

    List<StuffDto> res = stuffRepository.findAll();

    if (res.size() != 0) {
      return ResponseStatus.builder()
          .code("00")
          .message("SUCCESS")
          .description("Success get stuff")
          .response(res)
          .build();
    } else {
      return ResponseStatus.builder()
          .code("01")
          .message("FAILED")
          .description("Data not found")
          .build();
    }
  }

  // GET STUF BY NAME
  public ResponseStatus getStuffByName(String name) {
    StuffDto res = stuffRepository.getStuffByName(name);

    if (res != null) {
      return ResponseStatus.builder()
          .code("00")
          .description("Success get stuff")
          .message("SUCCESS")
          .response(res)
          .build();
    } else {
      return ResponseStatus.builder()
          .code("01")
          .message("FAILED")
          .description("Data not found")
          .build();
    }
  }

  // GET STUF BY ID
  public ResponseStatus getStuffById(Integer id) {
    log.info("Start Get Stuff by Id");
    log.info("Id: " + id);

    StuffDto res = stuffRepository.getStuffById(id);

    if (res != null) {
      return ResponseStatus.builder()
          .code("00")
          .description("Success get stuff")
          .message("SUCCESS")
          .response(res)
          .build();
    } else {
      return ResponseStatus.builder()
          .code("01")
          .message("FAILED")
          .description("Data not found")
          .build();
    }
  }
  
  // INSERT OR UPDATE STUFF
  public ResponseStatus insertOrUpdateStuff(StuffDto req) {
    log.info("Start Save Stuff");
    log.info(req.getId() != null ? "Execute Update Stuff" + req.getId() : "Insert Stuff");

    Integer id = req.getId() != null ? req.getId() : null;
    StuffDto res = new StuffDto();
    StuffDto stuffExist = new StuffDto();

    if (!id.equals(null)) {
      stuffExist = stuffRepository.getStuffById(id);
      stuffExist.setId(req.getId());
    }

    stuffExist.setName(req.getName());
    stuffExist.setPrice(req.getPrice());
    stuffExist.setStock(req.getStock());

    res = stuffRepository.save(stuffExist);

    if (res != null) {
      return ResponseStatus.builder()
          .code("00")
          .message("SUCCESS")
          .description("Success save stuff")
          .response(res)
          .build();
    } else {
      return ResponseStatus.builder()
          .code("01")
          .message("FAILED")
          .description("Failed save stuff")
          .build();
    }
  }

  public ResponseStatus insertOrUpdateSalesDetail(SalesDetailDto req) {
    log.info("Start Save Sales Detail");

    String resCode = "00";
    String resStatus = "SUCCESS";
    String resDesc = "Success save sales detail";

    SalesDetailDto res = salesDetailRepository.save(req);

    if (res == null) {
      resCode = "01";
      resStatus = "FAILED";
      resDesc = "Failes save sales detail";
    }

    return ResponseStatus.builder()
        .code(resCode)
        .message(resStatus)
        .description(resDesc)
        .response(res)
        .build();
  }
}
