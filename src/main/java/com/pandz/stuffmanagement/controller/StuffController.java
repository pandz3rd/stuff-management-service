package com.pandz.stuffmanagement.controller;

import com.pandz.stuffmanagement.dto.ResponseStatus;
import com.pandz.stuffmanagement.dto.SalesDetailDto;
import com.pandz.stuffmanagement.dto.StuffDto;
import com.pandz.stuffmanagement.repository.StuffRepository;
import com.pandz.stuffmanagement.service.StuffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static com.pandz.stuffmanagement.constant.GlobalConstant.*;

@RestController
public class StuffController {
  @Autowired
  private StuffService stuffService;

  @GetMapping(ALL_STUFF)
  public ResponseStatus getStuff() {
    return stuffService.getAllStuff();
  }

  @PostMapping(STUFF)
  public ResponseStatus getById(@RequestBody StuffDto payload) {
    Integer id = payload.getId() != null ? payload.getId() : null;

    System.out.println("Request: " + id);

    if (id == null) {
      return ResponseStatus.builder()
          .code("01")
          .message("BAD REQUEST")
          .description("Id is required")
          .build();
    }

    return stuffService.getStuffById(id);
  }

  @PostMapping(INSERT_STUFF)
  public ResponseStatus saveStuff(@RequestBody StuffDto req) {
    return stuffService.insertOrUpdateStuff(req);
  }

  // Insert sales detail
  @PostMapping(INSERT_SALES_DETAIL)
  public ResponseStatus saveSalesDetail(@RequestBody SalesDetailDto req) { return stuffService.insertOrUpdateSalesDetail(req); }
}
