package com.pandz.stuffmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StuffManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(StuffManagementApplication.class, args);
	}

}
