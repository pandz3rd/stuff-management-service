package com.pandz.stuffmanagement.constant;

public class GlobalConstant {
  // PATH
  public static final String STUFF = "/stuff";
  public static final String INSERT_STUFF = STUFF + "/save";
  public static final String ALL_STUFF = STUFF + "/all";

  public static final String SALES_DETAIL = "/salesdetail";
  public static final String INSERT_SALES_DETAIL = SALES_DETAIL + "/save";
}
