package com.pandz.stuffmanagement.repository;

import com.pandz.stuffmanagement.dto.SalesDetailDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalesDetailRepository extends JpaRepository<SalesDetailDto, String> {
}
