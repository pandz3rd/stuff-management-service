package com.pandz.stuffmanagement.repository;

import com.pandz.stuffmanagement.dto.StuffDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StuffRepository extends JpaRepository<StuffDto, String> {
  @Query(value = "SELECT * FROM m_stuff WHERE name = ?1", nativeQuery = true)
  StuffDto getStuffByName(String name);

  @Query(value = "SELECT * FROM m_stuff WHERE id = ?1", nativeQuery = true)
  StuffDto getStuffById(Integer id);
}
